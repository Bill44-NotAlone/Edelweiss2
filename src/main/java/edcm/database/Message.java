package edcm.database;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected long id;
    private long id_user;
    protected String text;

    public Message() {}
    public Message(int id_user, String text){
        this.id_user = id_user;
        this.text = text;
    }
}
