package edcm.database;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class UserApp {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected long id;
    protected String login;
    protected String password;

    public UserApp(){}

    public UserApp(String login, String password){
        this.login = login;
        this.password = password;
    }
}