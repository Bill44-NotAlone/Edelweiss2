package edcm.repos;

import edcm.database.UserApp;
import org.springframework.data.repository.CrudRepository;

public interface UserRepo extends CrudRepository<UserApp, Long> { }
