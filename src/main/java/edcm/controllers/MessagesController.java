package edcm.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import edcm.database.Message;
import edcm.repos.MessageRepo;
import edcm.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

@RestController
@RequestMapping("messages")
public class MessagesController {

    @Autowired
    private MessageRepo messageRepo;
    @Autowired
    private UserRepo userRepo;

    @GetMapping
    public Iterable<Message> messages(){
        return messageRepo.findAll();
    }

    @GetMapping(value = "/{id}")
    public Message getMessage(@PathVariable("id") Message message){
        return message;
    }

    @PutMapping(value = "/{id}/edit")
    public Message edit(@PathVariable("id") Message message, String text){
        message.setText(text);
        return messageRepo.save(message);
    }

    @GetMapping(value = "/filter")
    public List<Message> filter(@RequestParam String filterString) throws JsonProcessingException {
        List<String> words = Arrays.asList(filterString.split(" "));
        List<Message> messages = new ArrayList<>();
        messageRepo.findAll().forEach(message -> words.forEach(word -> {
            if (Pattern.matches(".*("+ word.toLowerCase() +").*", message.getText().toLowerCase()))
                messages.add(message);
        }));
        return messages;
    }

    @PostMapping
    private Message create(@RequestBody Message message){
        return messageRepo.save(message);
    }

    @DeleteMapping("{id}")
    public void delelte(@PathVariable("id") Message message){
        messageRepo.delete(message);
    }
}
